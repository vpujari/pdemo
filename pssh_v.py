try:
    import paramiko
    import threading
    import argparse
    import sys
    import logging
    import logging.handlers
    import time
except ImportError, err:
    print err

class ParamikoPssh(object):
    def __init__(self, uname=None, password=None):
        self._fname = '/home/vpujari/Desktop/gitid_rsa'
        self._maxthreads = 5
        self._cmd = 'uptime'
        self._uname = uname
        self._password = password
        self._host_list = list()
        self._host_groups = list()
        self._output = list() # For Debugging
        self.arguments_reader()
        self.host_grouping()
        self.assign_groups()
        print self._output
    
    def host_grouping(self):
        '''
        Diving the given host list into groups in order to assign to the threads.    
        TODO: It is giving extra list. i.e if threads are 5, it is creating 6 lists of hosts
        '''
        if len(self._host_list) < self._maxthreads:
            self._maxthreads = len(self._host_list)
        _each_group = len(self._host_list)/self._maxthreads
        _host_groups = [self._host_list[i:i+_each_group] for i in range(0, len(self._host_list), _each_group)]
        self._host_groups = _host_groups

    def assign_groups(self):
        '''
        Distribute host groups among threads.
        '''
        _threads = list()
        for _each in range(len(self._host_groups)):
            _job = threading.Thread(target = self.host_connect, args=[self._host_groups[_each]])
            _threads.append(_job)
            _job.start()
            
        for _each_thread in _threads:
            _each_thread.join()
        

    def host_connect(self,_host_group):
        '''
        Using Paramiko, it connects to the host and execute the given command.
        It uses id_rsa key to connect to host.
        '''
        ssh=paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        privkey=paramiko.RSAKey.from_private_key_file(self._fname)
        output = list() #For debugging purpose
        for _each_host in _host_group:
            ssh.connect('127.0.0.1',username='vpujari',pkey=privkey)
            stdin,stdout,stderr=ssh.exec_command(self._cmd)
            result = stdout.readlines()
            if result:
                self._output.append(result[0])
                time.sleep(2)

    def arguments_reader(self):
        parser = argparse.ArgumentParser(description='Apprunner', add_help=True)
        parser.add_argument('-H', action='store',default=None,help='Host to be connected to.')
        parser.add_argument('-HL',nargs='*', default=None,help='Muliple hosts to be connected to.')
        parser.add_argument('-c',action='store',default='uptime',help='Command to be executed on given hosts')
        parser.add_argument('-f',action='store',default='/home/vpujari/.ssh/id_rsa',help='The id_rsa to be passed while connecting to hosts')
        options = parser.parse_args()
        try:
            sys.argv[1]
        except:
            print "\n No arguments."
            parser.print_help()
            sys.exit(2)
            
        if options.HL is not None:
            self._host_list = options.HL
        
        if self._host_list and options.H:
            self._host_list.append(options.H)
            
        if options.c:
            self._cmd = options.c
            
        if options.f:
           self._fname = options.f
        #result = self.host_connect('127.0.0.1')
        #print result
              
    

if __name__=="__main__":
    p = ParamikoPssh('vpujari', '')
    
    
    
    
    
    
'''
 Command:
 python pssh_v.py -H 220.1.02.03 -HL 23.10.10.25 20.15.20.4
 
 Output:
 Namespace(H='220.1.02.03', HL=['23.10.10.25', '20.15.20.4'])
 
'''